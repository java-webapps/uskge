package ch.uskge;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UskgeController {

    @GetMapping("/")
    public String home(Model model) {
        return "index";
    }
}
