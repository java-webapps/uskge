<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html data-theme="light">
    <head>
        <title>Urban Sketchers of Geneva</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="img/uskge-inx.png">
        <link rel="stylesheet" href="font/fonts.css">
        <link rel="stylesheet" href="css/bulma.css">
        <link rel="stylesheet" href="css/stylesheet.css">
    </head>
    <body>
        <div class="container">
            <article class="media">
                <figure class="media-left is-hidden-mobile">
                    <p class="image is-128x128">
                        <img src="img/uskge-plain.svg" class="is-128x128">
                    </p>
                </figure>
                <div class="media-content">
                    <div class="content">
                        <div class="title is-size-1 has-text-left">
                            Urban Sketchers of Geneva
                        </div>
                        <div class="band">

                        </div>
                        <div class="columns is-desktop">
                            <div class="column">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare
                                magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa
                                sem. Etiam finibus odio quis feugiat facilisis.
                            </div>
                            <div class="column">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare
                                magna eros, eu pellentesque tortor vestibulum ut. Maecenas non massa
                                sem. Etiam finibus odio quis feugiat facilisis.
                            </div>
                        </p>
                    </div>
                </div>
            </article>
        </div>
    </body>
</html>
